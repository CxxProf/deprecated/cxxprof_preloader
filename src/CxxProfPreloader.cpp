
#include "cxxprof_preloader/CxxProfPreloader.h"
#include "cxxprof_preloader/IDynCxxProfProvider.h"

#include <exception>
#include <iostream>

namespace CxxProf
{
    const std::string PLUGINDIR = ".";

    CxxProfPreloader& CxxProfPreloader::getCxxProf()
    {
        std::cout << "Creating CxxProfPreloader" << std::endl;
        static CxxProfPreloader instance_;
        return instance_;
    }

    CxxProfPreloader::CxxProfPreloader() :
        manager_(new pluma::Pluma())
    {
        //load the plugin during instantiation of the CxxProfPreloader
        loadDynCxxProf();
    }

    CxxProfPreloader::~CxxProfPreloader()
    {}

    void CxxProfPreloader::loadDynCxxProf()
    {
        manager_->acceptProviderType< IDynCxxProfProvider >();
        manager_->loadFromFolder(PLUGINDIR);

        std::vector<IDynCxxProfProvider*> providers;
        manager_->getProviders(providers);

        //load all found plugins into our vector
        //all loaded plugins will be used together, so it is possible to have a ProfPlugin
        //storing the profiling data in a database while another sends it via
        //network to another application
        auto provIter = providers.begin();
        for (; provIter != providers.end(); ++provIter)
        {
            dynCxxProfs_.push_back((*provIter)->create());
            auto pluginIter = dynCxxProfs_.rbegin();
            std::cout << "Loaded DynCxxProf: " << (*pluginIter)->toString() << std::endl;
        }

        if (dynCxxProfs_.empty())
        {
            std::cout << "Cannot find Plugin, no DynCxxProf loaded..." << std::endl;
        }
    }

    void CxxProfPreloader::initialize()
    {
        //Nothing to do here for now
        //This part later could be used to initialize via configuration or something similar
    }

    std::vector< std::unique_ptr<IActivity> > CxxProfPreloader::createActivities(const std::string& name)
    {
        //this mutex protects the dynCxxProf_
        std::unique_lock<std::mutex> lock(mutex_);

        //this is the vector which contains all the activites our plugins are creating
        std::vector< std::unique_ptr<IActivity> > returnVec;

        //if no profilers are loaded, the following won't do anything...
        auto profIter = dynCxxProfs_.begin();
        for (; profIter != dynCxxProfs_.end(); ++profIter)
        {
            returnVec.push_back((*profIter)->createActivity(name));
        }

        return returnVec;
    }

    void CxxProfPreloader::addMark(const std::string& name)
    {
        //this mutex protects the dynCxxProf_
        std::unique_lock<std::mutex> lock(mutex_);

        //if no profilers are loaded, the following won't do anything...
        auto profIter = dynCxxProfs_.begin();
        for (; profIter != dynCxxProfs_.end(); ++profIter)
        {
            (*profIter)->addMark(name);
        }
    }

    void CxxProfPreloader::addPlotValue(const std::string& name, double value)
    {
        //this mutex protects the dynCxxProf_
        std::unique_lock<std::mutex> lock(mutex_);

        //if no profilers are loaded, the following won't do anything...
        auto profIter = dynCxxProfs_.begin();
        for (; profIter != dynCxxProfs_.end(); ++profIter)
        {
            (*profIter)->addPlotValue(name, value);
        }
    }

    void CxxProfPreloader::setProcessAlias(const std::string& name)
    {
        //this mutex protects the dynCxxProf_
        std::unique_lock<std::mutex> lock(mutex_);

        //if no profilers are loaded, the following won't do anything...
        auto profIter = dynCxxProfs_.begin();
        for (; profIter != dynCxxProfs_.end(); ++profIter)
        {
            (*profIter)->setProcessAlias(name);
        }
    }
    void CxxProfPreloader::setThreadAlias(const std::string& name)
    {
        //this mutex protects the dynCxxProf_
        std::unique_lock<std::mutex> lock(mutex_);

        //if no profilers are loaded, the following won't do anything...
        auto profIter = dynCxxProfs_.begin();
        for (; profIter != dynCxxProfs_.end(); ++profIter)
        {
            (*profIter)->setThreadAlias(name);
        }
    }

    void CxxProfPreloader::shutdown()
    {
        //this mutex protects the dynCxxProf_
        std::unique_lock<std::mutex> lock(mutex_);

        //if no profilers are loaded, the following won't do anything...
        auto profIter = dynCxxProfs_.begin();
        for (; profIter != dynCxxProfs_.end(); ++profIter)
        {
            (*profIter)->shutdown();
        }
    }

} //namespace CxxProf
