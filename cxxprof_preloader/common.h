
#pragma once

#ifdef WIN32
	#ifdef CXXPROF_PRELOADER
	    #define CxxProf_Preloader_EXPORT __declspec( dllexport )
	#else
	    #define CxxProf_Preloader_EXPORT __declspec( dllimport )
	#endif //CXXPROF_PRELOADER
#else
	#define CxxProf_Preloader_EXPORT 
#endif
