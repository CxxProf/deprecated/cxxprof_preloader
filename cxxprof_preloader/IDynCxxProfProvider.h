
#pragma once

#include <Pluma/Pluma.hpp>
#include "cxxprof_preloader/IDynCxxProf.h"

namespace CxxProf
{
    /**
     * This is needed for Pluma in order to load the DynCxxProf plugins
     */
    PLUMA_PROVIDER_HEADER(IDynCxxProf);
    PLUMA_PROVIDER_SOURCE(IDynCxxProf, 1, 1);

} //namespace CxxProf
