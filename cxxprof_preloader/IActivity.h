
#pragma once

#include "cxxprof_preloader/common.h"

namespace CxxProf
{
    /**
     * This interface acts as an interface to Activities.
     * As CxxProf let's the implementation decide on how Activities should behave,
     * this interface is mostly empty
     *
     * CxxProf just needs to know that there are Activities anywhere
     *
     */
    class CxxProf_Preloader_EXPORT IActivity
    {
    public:
        virtual ~IActivity() {};
    };

} //namespace CxxProf
